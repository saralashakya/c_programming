#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(){
printf("%lu",sizeof(int));
printf("%lu",sizeof(float));
printf("%lu",sizeof(char));
printf("Range of int: %d to %d\n", INT_MIN, INT_MAX);
printf("Range of float: %E to %E\n", FLT_MIN, FLT_MAX);
printf("Range of double: %E to %E\n", DBL_MIN, DBL_MAX);
printf("Range of char: %d to %d\n", CHAR_MIN, CHAR_MAX);

return 0;
}