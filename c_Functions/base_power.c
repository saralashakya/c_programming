#include <stdio.h>
int power(int, int);

int main(){
    int result = power(2,5);
    printf("%d",result);
    return 0;
}

int power (int base, int po){
    if(po < 1){
        return 1;
    }
    else{
        return (base * power(base,po - 1));
    }

}