#include <stdio.h>
void fun1();
int a = 5;
int b = 10;
int main(){
    int a = 1;
    fun1();
    {
        //Block scope
        int a = 10;
        printf("Inside block 1 %d\n",a);
    }
    {
        //Block scope
        printf("Inside block 2 %d\n",a);

    }
    printf("inside main %d\n",a);
    //program scope
    printf("%d\n",b);

    return 0;
}
void fun1(){
    int a = 23;
    printf("inside function %d\n",a);
}