#include <stdio.h>
int n,i;


void func() {
   int n = 100;
   i = 5;
}

int main() {
   func();
   printf("n= %d\n", n); // Esrror: n is defined here

   {
       int i = 1;
       printf("i = %d\n", i);

   }

   printf("i = %d\n", i); // Error: i is defined here
}

