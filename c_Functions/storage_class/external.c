#include <stdio.h>

int count = 0;

void readNumbers();

int main() {
   readNumbers();
   printf("You entered %d positive numbers\n", count);
}

void readNumbers() {
   int num;
   printf("Enter integers between 0 and 100:\n");
   do {
       scanf("%d", &num);
       if (num < 0) {
           printf("You entered a negative integer\n");
           break;
       }
       count++;
       printf("You entered %d.\n", num);
   } while (num <= 100);
}

