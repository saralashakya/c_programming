
#include <stdio.h>

int main(){
    int a = 10, b = 20, x, y;  
    x = ++a;
    y = b++;
    printf("x = %d, a = %d\n", x, a);		// x = 11, a = 11
    printf("y = %d, b = %d\n", y, b);		// y = 20, b = 21

    x = 50 - a++;			
    printf("x = %d, a = %d\n", x, a);		// ?

    y = 100 + --b;		
    printf("y = %d, b = %d\n", y, b);		// ?



}
