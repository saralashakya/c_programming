#include <stdio.h>

int main(){
    struct player{
        char name[20];
        int age;
        int run;
    };
    struct player p1={"Ram",30,300};
    struct player *p = &p1;
    printf("The information of player is as follows:\n");
    printf("Name: %s\t Age: %d\t Run: %d\t",p->name, p->age, p->run);

    return 0;
}