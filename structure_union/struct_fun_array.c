#include <stdio.h>

struct student
{
    int rollno;
    char name[80];
    int marks;
};

void accept(struct student list[], int s);
void display(struct student list[], int s);
void bsortDesc(struct student list[], int s);

int main()
{
    struct student data[20];
    int n;

    printf("Number of records you want to enter? : ");
    scanf("%d", &n);

    accept(data, n);
    printf("\nBefore sorting");
    display(data, n);
    bsortDesc(data, n);
    printf("\nAfter sorting");
    display(data, n);

    return 0;
} 

void accept(struct student list[80], int s)
{
    int i;
    for (i = 0; i < s; i++)
    {
        char a;
        printf("\n\nEnter data for Record #%d", i + 1);
        
        printf("\nEnter rollno : ");
        scanf("%d", &list[i].rollno);
        a= getchar();
        printf("Enter name : ");
        gets(list[i].name);

        printf("Enter marks : ");
        scanf("%d", &list[i].marks);
    } 
}

void display(struct student list[80], int s)
{
    int i;
    
    printf("\n\nRollno\tName\tMarks\n");
    for (i = 0; i < s; i++)
    {
        printf("%d\t%s\t%d\n", list[i].rollno, list[i].name, list[i].marks);
    } 
}

void bsortDesc(struct student list[80], int n)
{
    int i, j;
    struct student temp;
    
    for(int i = 0 ; i < n-1; i++){
        for(int j = i+1; j < n; j++){
            if(list[i].rollno > list[j].rollno){
                temp = list[i];
                list[i]=list[j];
                list[j]=temp;
            }
        }
      
        
    }

}