#include <stdio.h>
struct Student {
   int rollno;
   char name[30];
   float marks[3], average;
};
int main()
{
   int n = 3, sum = 0;
   struct Student students[n];

   for (int i = 0; i < n; i++)
   {
       printf("Enter details of student %d:\n", (i + 1));
       printf("Name: ");
       scanf("%s", students[i].name);
       printf("Roll number: ");
       scanf("%d", &students[i].rollno);

       printf("Enter marks obtained in \n");
       printf("Science: ");
       scanf("%f", &students[i].marks[0]);
       printf("Maths: ");
       scanf("%f", &students[i].marks[1]);
       printf("Computer programming: ");
       scanf("%f", &students[i].marks[2]);
   }

   for (int i = 0; i < n; i++)
   {
       sum = 0;
       for (int j = 0; j < 3; j++)
       {
           sum += students[i].marks[j];
       }
       students[i].average = sum / 3.0;
   }

   for (int i = 0; i < n; i++)
   {
       printf("%s got %0.2f.\n", students[i].name, students[i].average);
   }
}
