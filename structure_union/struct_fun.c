#include <stdio.h>
struct book{
    char name[20];
    int page;
    int price;

};
void print_book_info(struct book);

int main(){

    struct book b1;
    printf("Enter name of book");
    gets(b1.name);
    printf("Enter no of pages in book");
    scanf("%d",&b1.page);
    printf("Enter price of  book");
    scanf("%d",&b1.price);
    print_book_info(b1);
    return 0; 
}

void print_book_info(struct book b1){
    printf("The information of book is as follows:\n");
    printf("Book Name: %s \t Book pages: %d\t Book Price %d\t", b1.name, b1.page,b1.price);
    
}