#include <stdio.h>
#define PI 3.14
int main(){
    float radius;
    char choice;
    printf("Enter the radius: \n");
    scanf("%f", &radius);    // %*c will read the newline from the buffer and discard it
    printf("Type 'C' to calculate circumference, or type 'A' to calculate area\n");
    scanf(" %c",&choice);
    switch(choice) {
    case 'C':
        printf("Circumference is %f.\n", 2 * PI * radius);
        break;
    case 'A':
        printf("Area is %f.\n", PI * radius * radius);
        break;
    default:
        printf("Invalid choice.\n");
    }

}
