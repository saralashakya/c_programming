#include <stdio.h>

int main() {
    int num;

retry:
    printf("Enter a positive number: ");
    scanf("%d", &num);

    if (num <= 0) {
        printf("Invalid input. Please enter a positive number.\n");
        goto retry;
    }

    printf("You entered: %d\n", num);

    return 0;
}