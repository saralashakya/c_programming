// C program to illustrate fread() function
// when size of the file or the value of count is equal to 0

#include <stdio.h>

int main()
{
	// File pointer
	FILE* filePointer;
	// Buffer to store the read data
	char buffer[100];
	// "g4g.txt" file is opened in read mode

	filePointer = fopen("data.txt", "r");
	// Case when count is equal to 0
	printf("count = 0, return value = %zu\n",
		fread(buffer, sizeof(buffer), 0, filePointer));
	// Case when size is equal to 0
	printf("size = 0, return value = %zu\n",
		fread(buffer, 0, 1, filePointer));
	return 0;
}
