#include <stdio.h>
#include <stdlib.h>
int main(){
    FILE *fptr;
    char item[10];
    int qty, rate , i;
    fptr= fopen("inventory.txt","r");

    if(fptr == NULL){
        printf("File cannot be opened");
        exit(0);
    }

    printf("Item\t Qty\tRate\tTotal Amount\n");
    printf("----------------------------------------\n");
    for(i = 0; i< 3; i++){
        fscanf(fptr,"%s",item);
        fscanf(fptr,"%d%d",&qty,&rate);
        printf("%s\t%d\t%d\t%d\n",item,qty,rate,qty*rate);
    }
    fclose(fptr);
    return 0;

}