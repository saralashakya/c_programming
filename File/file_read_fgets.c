#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE_LENGTH 1024

int main()
{
    // file pointer variable to store the value returned by fopen
    FILE* fptr;
    char line[MAX_LINE_LENGTH];

    // opening the file in read mode
    fptr = fopen("Axe_Band/Chiya_Bari.txt", "r");

    // checking if the file is opened successfully
    if (fptr == NULL) {
        printf("The file is not opened. The program will now exit.");
        exit(0);
    }
     // reading the content of the file and printing it to the console
    while (fgets(line, sizeof(line), fptr) != NULL) {
        printf("%s", line);
    }
    // closing the file
    fclose(fptr);
    return 0;
}
