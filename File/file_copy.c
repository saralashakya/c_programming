#include <stdio.h>

int main(){
    char ch;
    FILE *fp1, *fp2;
    fp1 = fopen("data.txt","w");
    ch = getchar();
    //writing to file data.txt
    while(ch!='\n'){
        fputc(ch,fp1);
        ch = getchar();

    }
    fclose(fp1);
    fp1=fopen("data.txt","r");
    fp2=fopen("data1.txt","w");

    ch = fgetc(fp1);
    while (ch != EOF) {
        printf("%c", ch);
        ch = fgetc(fp1);
        fputc(ch,fp2);
    }
    fclose(fp1);
    fclose(fp2);
    fp2 = fopen("data1.txt","r");
    ch = fgetc(fp2);
    while(ch!=EOF){
        printf("%c",ch);
        ch = fgetc(fp2);
    }
    return 0;
}