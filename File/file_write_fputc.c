#include <stdio.h>
int main(){
   FILE *fp;
   char ch;
   //writing to file data.txt
   fp = fopen("data.txt","w");
   printf("Enter a text to store data in file");
   ch = getchar();
   while(ch!= '\n'){
       fputc(ch,fp);
       ch = getchar();
   }
   fclose(fp);

//reading from file data.txt
   fp = fopen("data.txt","r");
   ch = fgetc(fp);
   while(ch!= EOF){
       printf("%c",ch);
       ch=fgetc(fp);

   }
   fclose(fp);

   return 0;
}

