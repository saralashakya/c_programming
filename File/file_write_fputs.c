#include <stdio.h>

int main(){
   FILE *fp;
   char ch[100];
   //open the file in write mode and write the content in the file
   fp = fopen("data1.txt","w");
   printf("Enter a text that you want to store");
   gets(ch);
   fputs(ch,fp);
   fclose(fp);
  

// Open the file in read mode
   fp = fopen("data1.txt", "r");
   if (fp == NULL) {
       perror("Error opening file for reading");
       return 1;
   }
   // Read and print the contents of the file
   while (fgets(ch, sizeof(ch), fp) != NULL) {
       printf("%s", ch);
   }
   // Close the file after reading
   fclose(fp);
   return 0;
}

