#include <stdio.h>
#include <stdlib.h>

int main(){
    FILE *fptr;
    char items[10];
    int qty, rate , i;
    fptr = fopen("inventory.txt","w");
    if(fptr == NULL){
        printf("File cannot be created");
        exit(1);
    }
    for(int i = 0; i < 3; i++){
        printf("Enter product name, qty and rate\t");
        scanf("%s",items);
        scanf("%d%d",&qty,&rate);
        fprintf(fptr,"%s\t%d\t%d\n",items,qty,rate);
    }
    fclose(fptr);
    return 0;
}