#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char name[20];
    int quantity;
    float price;
} Product;

void writeProducts(const char *filename, Product *products, size_t count) {
    FILE *fptr = fopen(filename, "w+");
    if (fptr == NULL) {
        printf("File cannot be opened for writing\n");
        exit(1);
    }

    fwrite(products, sizeof(Product), count, fptr);
    fclose(fptr);
}
void readProducts(const char *filename, Product *products, size_t count) {
    FILE *fptr = fopen(filename, "r+");
    if (fptr == NULL) {
        printf("File cannot be opened for reading\n");
        exit(1);
    }

    fread(products, sizeof(Product), count, fptr);
    fclose(fptr);
}
int main() {
    Product products[3] = {
        {"Apples", 10, 0.5},
        {"Oranges", 20, 0.3},
        {"Bananas", 15, 0.2}
    };

    const char *filename = "products.txt";

    // Write the products to a file
    writeProducts(filename, products, 3);

    // Read the products back from the file
    Product readProductsArray[3];
    readProducts(filename, readProductsArray, 3);

    // Display the read products
    printf("Products read from file:\n");
    for (int i = 0; i < 3; i++) {
        printf("Name: %s, Quantity: %d, Price: %.2f\n",
               readProductsArray[i].name, readProductsArray[i].quantity, readProductsArray[i].price);
    }

    return 0;
}