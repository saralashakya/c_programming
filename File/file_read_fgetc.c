// C Program to illustrate file opening
#include <stdio.h>
#include <stdlib.h>
int main(){
	// file pointer variable to store the value returned by fopen
	FILE* fptr;
    char c;
	// opening the file in read mode
	fptr = fopen("Axe_Band/roads.csv", "r");
	// checking if the file is opened successfully
	if (fptr == NULL) {
		printf("The file is not opened. The program will now exit.");
		exit(0);
	}
     // reading the content of the file and printing it to the console
    c = fgetc(fptr);
    while (c != EOF) {
        printf("%c", c);
        c = fgetc(fptr);
    }
    // closing the file
    fclose(fptr);
	return 0;
}
