#include <stdio.h>
#define n 5

int main(){
    int arr[n], large, small;
    printf("Enter the elements in an array\n");
    for(int i =0;i<5;i++){
        printf("Enter the value of element %d",i+1);
        scanf("%d",&arr[i]);
    }
    printf("Value in the array are\n");

    for(int i =0;i<5;i++){
        printf("%d\t",arr[i]);    
    }

    large = arr[0];
    small = arr[0];
    for(int i = 1; i < 5; i++){
        //to find the largest number in an array
        if(large < arr[i]){
            large = arr[i];
        }
        //to find the smallest number in an array
        if(small > arr[i]){
            small = arr[i];
        }

    }
    printf("\nThe largest element is %d and smallest number is %d", large,small);
    return 0;
}