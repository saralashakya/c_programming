#include <stdio.h>
int main() {
   float marks[100], avg = 0;
   int n;
   printf("How many students?\n");
   scanf("%d", &n);

   for (int i = 0; i < n; i++) {
       printf("Enter the marks obtained by student %d: ", i + 1);
       scanf("%f", &marks[i]);
   }
   for (int i = 0; i < n; i++) {
       avg += marks[i];
   }
   avg /= n;

   printf("Average = %f\n", avg);
}
