
#include <stdio.h>
#include <string.h>

int main() {
    char a[30] = "Hello ";
    char b[30] = "How are you?";
    printf("The source string is %s\n",a);
    printf("The destination string is %s\n",b);
    strcat(a,b);
    printf("The combined string is %s",a);
}