#include <stdio.h>
#define N 3
#define M 4
int main(){
    int mat1[N][M], mat2[N][M], mat3[N][M];
    printf("Enter the elements of matrix 1\n");
    //scanning the elements of matrix 1
    for(int i = 0; i<3; i++){
        for(int j = 0; j<4; j++){
            printf("Enter the value of mat1[%d][%d]",i,j);
            scanf("%d",&mat1[i][j]);
        }
    }
    //printing the elements of matrix 1
    for(int i = 0; i<3; i++){
        for(int j = 0; j<4; j++){
            printf("%d ",mat1[i][j]);
            
        }
        printf("\n ");
    }
    //scanning the elements of matrix 2
    for(int i = 0; i<3; i++){
        for(int j = 0; j<4; j++){
            printf("Enter the value of mat2[%d][%d]",i,j);
            scanf("%d",&mat2[i][j]);
        }
    }
    //printing the elements of matrix 2
    for(int i = 0; i<3; i++){
        for(int j = 0; j<4; j++){
            printf("%d ",mat2[i][j]);        
        }
        printf("\n");
    }
    //adding the elements of matrix 1  and matrix 2 and storing it in matrix 3
    for(int i = 0; i<3; i++){
        for(int j = 0; j<4; j++){
           mat3[i][j] = mat1[i][j] + mat2[i][j];
            
        }
    }
    //printing the elements of matrix 3

    for(int i = 0; i<3; i++){
        for(int j = 0; j<4; j++){
           printf("%d",mat3[i][j]);
            
        }
        printf("\n");
    }

    return 0;
}