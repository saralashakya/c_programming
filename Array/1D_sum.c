#include <stdio.h>
#define n 5

int main(){
    int arr[n], sum = 0;
    printf("Enter the elements in an array\n");
    for(int i =0;i<5;i++){
        printf("Enter the value of element %d",i+1);
        scanf("%d",&arr[i]);
    }
    printf("Value in the array are\n");

    for(int i =0;i<5;i++){
        printf("%d\t",arr[i]);
        sum = sum + arr[i];
        
    }
    printf("The sum of an array is %d", sum);
    return 0;
}