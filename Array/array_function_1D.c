#include <stdio.h>

float average(float[], int);

int main() {

   float marks[100];
   int n;

   printf("How many students?\n");
   scanf("%d", &n);

   for (int i = 0; i < n; i++) {
       printf("Enter the marks obtained by student %d: ", i + 1);
       scanf("%f", &marks[i]);
   }

   printf("Average = %f\n", average(marks, n));
}

float average(float a[], int n) {
   float avg = 0;
  
   for (int i = 0; i < n; i++){
       avg += a[i];
   }
   avg /= n;

   return avg;
}

