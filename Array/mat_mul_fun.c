#include <stdio.h>
#include <string.h>
void scan_element(int mat1[][2]);
void print_element(int mat1[][2]);
void mat_multi(int mat1[][2],int mat2[][2],int mat3[][2]);
int main(){
    int mat1[2][2], mat2[2][2], mat3[2][2];
    printf("Enter elements of matrix 1");
    scan_element(mat1);
    printf("Enter elements of matrix 2");
    scan_element(mat2);
    printf("Matrix 1 element\n");
    print_element(mat1);
    printf("Matrix 2 element\n");
    print_element(mat2);
    printf("Matrix 3 element\n");
    mat_multi(mat1,mat2,mat3);
    print_element(mat3);
    return 0;
}

void scan_element(int mat1[][2]){
    for(int i =0;i<2;i++){
        for(int j=0;j<2;j++){
            scanf("%d",&mat1[i][j]);
        }
    }

}
void print_element(int mat1[][2]){
    for(int i =0;i<2;i++){
        for(int j=0;j<2;j++){
            printf("%d",mat1[i][j]);
        }
        printf("\n");
    }
}
void mat_multi(int mat1[][2],int mat2[][2],int mat3[][2]){
    int sum;
    for(int i =0;i<2;i++){ 
        for(int j=0;j<2;j++){
            sum = 0;
            for(int k =0; k<2;k++){
                sum = sum+mat1[i][k]*mat2[k][j];
            }
            mat3[i][j] = sum;
        }
    }

}

