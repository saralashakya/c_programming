#include <stdio.h>
#include <string.h>
void strrev(char a[], char rev[]);

int main() {
   char a[20];
   char rev[20];
   printf("Enter String");
   gets(a);
   printf("The entered string is %s\n",a);
   strrev(a,rev);
   printf("The reversed string is %s",rev);
   return 0;
}

void strrev(char a[],char rev[]){
    int start = 0;
    int len = strlen(a);
    for(int i = len-1; i >=0; i--){
        rev[start] = a[i];
        start++;
    }

printf("The reversed string is %s",rev);
}

