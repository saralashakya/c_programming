#include <stdio.h>
#define ROWS 3
#define COLS 4

int main() {
   int matrix1[ROWS][COLS] = {
                               {1, 2, 3, 4},
                               {1, 2, 3, 4},
                               {1, 2, 3, 4}
                             };
   for (int i = 0; i < ROWS; i++) {
       for (int j = 0; j < COLS; j++) {
           printf("Value of Element [%d][%d] is %d\n",i,j,matrix1[i][j]);
           printf("Element [%d][%d] is at %p\n", i, j, &matrix1[i][j]);
       }
   }
   printf("\n");
}
