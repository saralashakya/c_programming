#include <stdio.h>

int main(){
    char a[20], b[20];
    printf("Enter the string: ");
    gets(a);
    int i = 0;
    while(a[i] != '\0') {
        b[i] = a[i];
        i++;
    }
    b[i] = '\0'; // Terminate the copied string with a null terminator
    printf("The copied string is %s\n", b);

    return 0;
}