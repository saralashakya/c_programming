#include <stdio.h>

int main(){
    int a[10]= {1,5,6,7,8,3,5,6,5,4};
    int n = 10, temp, i, j;
    int * p = a;

    for(i = 0; i< n - 1; i++){
        for(j = i+1; j<n; j++){
            if(*(p+i) > *(p+j)){
                temp = *(p+i);
                *(p+i) = *(p+j);
                *(p+j) = temp;
            }

        }

    }

    return 0;
}
