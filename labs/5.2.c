#include <stdio.h>
int fact(int n);

int main(){
    int n;
    printf("Enter the number to find its factorial");
    scanf("%d",&n);
    int factorial = fact(n);
    printf("The factorial is %d",factorial);
    return 0;
}

int fact(int n){
    if(n == 1)
        return 1;
    else
        return (n* fact(n-1));

}