#include <stdio.h>
void convert_case(char *c);

int main(){
    char a;
    scanf("%c",&a);
    printf("Before converting case\n");
    printf("%c",a);
    convert_case(&a);
    printf("\nAfter converting case\n");
    printf("%c",a);
    return 0;
}

void convert_case(char *c){
    if (*c >= 'A' && *c <= 'Z') {
        *c = *c + 32; // converting uppercase to lowercase
    }
    else{
        *c = *c - 32; // converting lowercase to uppercase
    }

}
