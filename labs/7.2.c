#include <stdio.h>
#define size 20
void find_large_small(int arr[], int n);
int main()
{
    int num[size], sum = 0;
    int n;
    printf("How many numbers?\n");
    scanf("%d", &n);

    // Scanning the numbers from the user
    for (int i = 0; i < n; i++)
    {
        printf("Enter number %d: ", i + 1);
        scanf("%d", &num[i]);
    }
    find_large_small(num, n);

    return 0;
}
void find_large_small(int arr[], int n)
{
    int largest = arr[0];
    int smallest = arr[0];
    // finding the largest and smallest number
    for (int i = 1; i < n; i++)
    {

        if (largest < arr[i])
        {
            largest = arr[i];
        }

        if (smallest > arr[i])
        {
            smallest = arr[i];
        }
    }

    printf("Largest = %d\n", largest);
    printf("Smallest = %d\n", smallest);
}
