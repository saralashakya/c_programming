#include <stdio.h>
struct student{
    int roll;
    char name[50];
    char address[100];
};

int main(){
    struct student s1[5];

    printf("Enter the information of the student\n");
    for(int i = 0 ; i < 5; i++){
        printf("Enter roll of student");
        scanf("%d",&s1[i].roll);
        printf("Enter name of student");
        scanf("%s",s1[i].name);
        printf("Enter address of student");
        scanf("%s",s1[i].address);
    }
    
    printf("The information of the student is as follows:\n");
    for(int i = 0; i< 5; i++){
        printf("%d\t%s\t%s",s1[i].roll,s1[i].name, s1[i].address);
        printf("\n");

    }

    return 0;
}