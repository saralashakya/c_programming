#include <stdio.h>

int main() {
    int a = 5, b = 10, c = 15, result1, result2, result3;

    result1 = ++a + b++ - --c; 
    printf("Result 1: %d\n", result1);
    printf("a: %d, b: %d, c: %d\n\n", a, b, c);

    result2 = b++ - --a + ++c;  
    printf("Result 2: %d\n", result2);
    printf("a: %d, b: %d, c: %d\n\n", a, b, c);

    result3 = c++ + ++b - a--;  
    printf("Result 3: %d\n", result3);
    printf("a: %d, b: %d, c: %d\n\n", a, b, c);
    return 0;
}
