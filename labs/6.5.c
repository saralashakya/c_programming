#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 10 // Maximum size of matrices


int main(){
    int mat1[MAX_SIZE][MAX_SIZE];
    int mat2[MAX_SIZE][MAX_SIZE];
    int mat3[MAX_SIZE][MAX_SIZE];
    int i,j,sum=0,r1,c1,r2,c2;

    printf("Enter the number of rows and columns for the first matrix: ");
    scanf("%d %d", &r1, &c1);

    printf("Enter the number of rows and columns for the second matrix: ");
    scanf("%d %d", &r2, &c2);

    if (c1 != r2) {
        printf("Error! Number of columns in the first matrix should be equal to the number of rows in the second matrix.\n");
        exit(0); 
    }
 


    //scanning elements of mat1
    printf("Enter the elements of matrix 1\n");
    for(i = 0; i < r1; i++){
        for(int j = 0; j< c1; j++ ){
            printf("Enter the element of mat1[%d][%d]",i,j);
            scanf("%d",&mat1[i][j]);

        }

    }
    //scanning elements of mat2
    printf("Enter the elements of matrix 2\n");
    for(i = 0; i < r2; i++){
        for(j = 0; j< c2; j++ ){
            printf("Enter the element of mat2[%d][%d]",i,j);
            scanf("%d",&mat2[i][j]);

        }

    }

    //performing matrix multiplication
    //resultant matrix will be of r1 and c2. so iterating loop of i to r1 and that of j to c2
    for(i = 0;i < r1;i++){
        for(j=0; j< c2;j++ ){
            sum = 0;
            for(int k=0;k<r2;k++){ 
                sum = sum + mat1[i][k]*mat2[k][j];
        }
        mat3[i][j]=sum;

    }
 
}

//printing mat1
    printf("Matrix 1\n");
    for(i = 0; i < r1; i++){
        for(j = 0; j< c1; j++ ){
            printf("%d",mat1[i][j]);
        }
        printf("\n");

    }

//printing mat2
    printf("Matrix 2\n");
    for(i = 0; i < r2; i++){
        for(j = 0; j< c2; j++ ){
            printf("%d",mat2[i][j]);
        }
        printf("\n");

    }
//printing mat3
    printf("Matrix 3\n");
    for(i = 0; i < r1; i++){
        for(j = 0; j< c2; j++ ){
            printf("%d",mat3[i][j]);
        }
        printf("\n");

    }
return 0;
}
