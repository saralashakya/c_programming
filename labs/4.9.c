// Write a program to find sum of series 
// 1!/1 + 2!/2 + 3!/3 + 4!/4 …….n!/n

#include <stdio.h>

int main(){
    int n, sum = 0;
    printf("Enter the number n to find the sum of its series");
    scanf("%d",&n);

    for (int i = 1;i <= n; i++){
        unsigned long int fact = 1;
        for(int j = 1; j<=i;j++){
            fact = fact * j;
        }
        sum = sum+ (fact)/i;

    }
    printf("sum is %d",sum);

    return 0;
}