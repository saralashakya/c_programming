//write a program to input 10 numbers in an array and print them
//  in an ascending order using array name itself as a pointer


#include <stdio.h>

int main(){
    int a[10]= {1,5,6,7,8,3,5,6,5,4};
    int n = 10, temp, i, j;

    for(i = 0; i< n - 1; i++){
        for(j = i+1; j<n; j++){
            if(*(a+i) > *(a+j)){
                temp = *(a+i);
                *(a+i) = *(a+j);
                *(a+j) = temp;
            }

        }

    }

    for(int i = 0; i< n; i++){
        printf("%d\t",a[i]);
    }

    return 0;
}
