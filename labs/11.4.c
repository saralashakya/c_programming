// WAP to enter a line of text and store them in file a.txt.
// Copy the content of the file a.txt to new file b.txt.
//  Finally read the content of the new file b.txt.

#include <stdio.h>

int main()
{
    FILE *fptr,*fptr1;
    char ch[40];
    fptr = fopen("a.txt", "w");
    printf("Enter a line of text that you want to store");
    gets(ch);
    fputs(ch, fptr);
    fclose(fptr);

    fptr = fopen("a.txt", "r");
    fptr1=fopen("b.txt","w");

    while(fgets(ch,sizeof(ch),fptr)!=NULL){
        fputs(ch,fptr1);
    }
    fclose(fptr);
    fclose(fptr1);
    fptr1=fopen("b.txt","r");

    while(fgets(ch,sizeof(ch),fptr1)!=NULL){
        printf("%s",ch);
    }

    fclose(fptr1);


    return 0;
}