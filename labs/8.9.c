// WAP that prompts the user to enter a matrix of order 4 X 4 
// and find the sum of the diagonal element using a pointer. 

#include <stdio.h>
int main(){
     int mat[4][4]={{1,1,1,1},{1,1,1,1,},{1,1,1,1},{1,1,1,1}},sum = 0;
     int (*p)[4] =mat;

     for(int i = 0; i < 2; i++){
        for(int j = 0; j < 2; j++){
            printf("%u\n", *(*(p+i)+j));
        }
     }

     for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
           if(i == j){
            sum = sum + *((*(p+i))+j);
           }
        }
     }

     printf("The sum of diagonal element is %d", sum);



    return 0;
}