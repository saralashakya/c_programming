
#include <stdio.h>
void swap(int a, int b);

int main(){
    int a = 5, b = 10;
    printf("The value of a is %d and b is %d before swap\n",a,b);
    swap(a,b);
    printf("Even after calling swap function, the value of a is %d b is %d in main function\n",a,b);
    return 0;
}

void swap(int a, int b){
    int temp = a;
    a = b;
    b = temp;
    printf("The value of a is %d b is %d in swap function\n",a,b);

}