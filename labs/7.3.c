#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 10 // Maximum size of matrices

void scan_element(int a[][MAX_SIZE], int rows, int cols); 
void print_element(int a[][MAX_SIZE], int rows, int cols);
void mat_multiplication(int a[][MAX_SIZE], int b[][MAX_SIZE], int c[][MAX_SIZE], int row1, int col1, int row2, int col2);

int main()
{
    int matrix1[MAX_SIZE][MAX_SIZE], matrix2[MAX_SIZE][MAX_SIZE], matrix3[MAX_SIZE][MAX_SIZE];
    int rows1, cols1, rows2, cols2;

    printf("Enter the number of rows and columns for the first matrix: ");
    scanf("%d%d", &rows1,&cols1);

    printf("Enter the number of rows and columns for the second matrix: ");
    scanf("%d%d", &rows2,&cols2);

    // Check if the column of the first matrix is equal to the row to second matrix
    if (cols1 != rows2) {
        printf("Error! Number of columns in the first matrix should be equal to the number of rows in the second matrix.\n");
        exit(0); 
    }
    // Input for the first matrix
    printf("Enter elements of the first matrix:\n");
    scan_element(matrix1, rows1, cols1);

    // Input for the second matrix
    printf("Enter elements of the second matrix:\n");
    scan_element(matrix2, rows2, cols2);

    // Printing first matrix
    printf("First matrix:\n");
    print_element(matrix1, rows1, cols1);

    // Printing second matrix
    printf("Second matrix:\n");
    print_element(matrix2, rows2, cols2);

    // Multiplying first and second matrix
    mat_multiplication(matrix1, matrix2, matrix3, rows1, cols1, rows2, cols2);
  
    // Displaying the resultant multiplied matrix
    printf("Resultant multiplied matrix:\n");
    print_element(matrix3, rows1, cols2);

    return 0;
}

void scan_element(int a[][MAX_SIZE], int rows, int cols){
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            scanf("%d", &a[i][j]);
        }
    }
}

void print_element(int a[][MAX_SIZE], int rows, int cols){
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
}

void mat_multiplication(int a[][MAX_SIZE], int b[][MAX_SIZE], int c[][MAX_SIZE], int row1, int col1, int row2, int col2){
    int sum;
    for(int i = 0; i < row1; i++){
        for(int j = 0; j < col2; j++ ){
            sum = 0;
            for(int k = 0; k < row2; k++){ 
                sum = sum + a[i][k] * b[k][j];
            }
            c[i][j] = sum;
        }
    }
}
