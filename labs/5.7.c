#include <stdio.h>
int power(int,int);

int  main(){
int x = 2, y = 3;
int result = power(x,y);
printf("%d power %d is %d",x,y,result);
    return 0;
}

int power(int x,int y){
    if(y == 0)
        return 1;
    else{
        return x * power(x,y-1);
    }
    
}