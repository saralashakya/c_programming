#include <stdio.h>
#define size 20
int main()
{
    int num[size], sum = 0;
    int n;
    printf("How many numbers?\n");
    scanf("%d", &n);

    // Scanning the numbers from the user
    for (int i = 0; i < n; i++)
    {
        printf("Enter number %d: ", i + 1);
        scanf("%d", &num[i]);
    }

    int largest = num[0];
    int smallest = num[0];
    // finding the largest and smallest number
    for (int i = 1; i < n; i++)
    {

        if (largest < num[i])
        {
            largest = num[i];
        }

        if (smallest > num[i])
        {
            smallest = num[i];
        }
    }

    printf("Largest = %d\n", largest);
    printf("Smallest = %d\n", smallest);


    return 0;
}