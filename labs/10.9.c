#include <stdio.h>
int  main()
{
    union value
    {
        int x;
        double y;
    };
    union value v;
    v.x = 10;
    v.y = 20.0;
    printf("The value of union member x is %d and y is % lf\n", v.x, v.y);
    printf("The total memory occupied by the union is %lu", sizeof(v));
    return 0;
}