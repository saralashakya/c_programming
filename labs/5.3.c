// WAP to find sum of series sum = 1+2+3+4+..............+n using normal function. 
// Prompt the user to input the n. Pass the n to a function to calculate the sum of its series 
// and then return that value to the main function.

#include <stdio.h>
int sum_series(int n);

int main(){
    int n, result;
    printf("Enter value of n to find sum of series");
    scanf("%d",&n);
    result = sum_series(n);
    printf("The sum of series up to %d is %d",n,result);
    return 0;
}

int sum_series(int n){
    int sum = 0;
    for(int i =1;i<=n;i++){
        sum = sum + i;
        
    }
    return sum;

}
