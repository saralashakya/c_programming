#include <stdio.h>
struct student{
    int roll;
    char name[50];
    char address[100];
};

int main(){
    struct student s1;
    printf("Enter roll of student");
    scanf("%d",&s1.roll);
    printf("Enter name of student");
    scanf("%s",s1.name);
    printf("Enter address of student");
    scanf("%s",s1.address);
    printf("The information of the student is as follows:\n");
    printf("%d\t%s\t%s",s1.roll,s1.name, s1.address);

    return 0;
}