
#include <stdio.h>
int sum_factorial(int arr[], int n);
#define size 20
int main() {
   int num[size];
   int n;
   printf("How many numbers?\n");
   scanf("%d",&n);

   // Scanning the numbers from the user
   for (int i = 0; i < n; i++) {
       printf("Enter number %d: ", i + 1);
       scanf("%d", &num[i]);
   }

   printf("Sum = %d\n", sum_factorial(num,n));
   return 0;
}

  int sum_factorial(int arr[], int n){
    int sum = 0;
    // Calculating the sum of factorials of each number
    for (int i = 0; i < n; i++) {
       int fact = 1;
       // Calculating factorial
       for(int j = 1; j <= arr[i]; j++) {
            fact = fact * j;
       }
       // Adding factorial to the sum
       sum = sum + fact;
   }
    return sum;

  }
