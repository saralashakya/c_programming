#include <stdio.h>
struct student{
    char name[30];
    int roll;
    int marks;
};

int main(){
    struct student s;
    FILE *fp = fopen("student.bin","wb");

    for(int i = 0; i< 4; i++){
        printf("Enter name, roll and marks of student %d\n",i+1);
        scanf("%s%d%d",s.name,&s.roll,&s.marks);
        fwrite(&s,sizeof(s),1,fp);
    }
    fclose(fp);
    fp = fopen("student.bin","rb");
    while(fread(&s,sizeof(s),1,fp)==1){
        printf("%s\t%d\t%d\n",s.name,s.roll,s.marks);

    }
    fclose(fp);

    return 0;
}