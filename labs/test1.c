#include <stdio.h>

struct student {
    char name[30];
    int roll;
    int marks;
};

int main() {
    struct student s[5];
    FILE *fp = fopen("student.bin", "wb");

    if (fp == NULL) {
        printf("Error opening file for writing.\n");
        return 1;
    }

    for (int i = 0; i < 4; i++) {
        printf("Enter name, roll and marks of student %d\n", i + 1);
        scanf("%s%d%d", s[i].name, &s[i].roll, &s[i].marks);
    }

    fwrite(s, sizeof(struct student), 4, fp);  // Writing only the 4 filled elements
    fclose(fp);

    fp = fopen("student.bin", "rb");

    if (fp == NULL) {
        printf("Error opening file for reading.\n");
        return 1;
    }

    fread(s, sizeof(struct student), 4, fp);  // Reading the 4 elements back

    for (int i = 0; i < 4; i++) {
        printf("%s\t%d\t%d\n", s[i].name, s[i].roll, s[i].marks);
    }

    fclose(fp);

    return 0;
}
