// WAP to store the information (as mentioned in qno. 8) 
// of 5 students and and display the information of the 
// student in ascending order based on their age

#include <stdio.h>
struct student{
    int roll;
    char name[50];
    char address[100];
};
int main(){
    int n = 5;
    struct student s1[n], temp;
    for(int i = 0; i < n; i++){
        printf("Enter information of student %d\n",i+1);
        printf("Enter roll no");
        scanf("%d",&s1[i].roll);
        printf("Enter name");
        char a = getchar();
        scanf("%s",s1[i].name);
        printf("Enter address");
        a = getchar();
        scanf("%s",s1[i].address);

    }

    for(int i = 0; i < n; i++){ 
        printf("roll no: %d\t Name: %s\t Address: %s\n",s1[i].roll,s1[i].name,s1[i].address);
    }

    for(int i = 0; i <n-1; i++){
        for(int j = i+1; j < n; j++){
            if(s1[i].roll > s1[j].roll){
                temp = s1[i];
                s1[i] = s1[j];
                s1[j] = temp;
            }
        }
    }
    for(int i = 0; i < n; i++){   
        printf("roll no: %d\t Name: %s\t Address: %s\n",s1[i].roll,s1[i].name,s1[i].address);
    }
    
    return 0;
}