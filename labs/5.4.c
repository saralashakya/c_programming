

#include <stdio.h>
int sum_series(int n);

int main(){
    int n, result;
    printf("Enter value of n to find sum of series");
    scanf("%d",&n);
    result = sum_series(n);
    printf("The sum of series up to %d is %d",n,result);
    return 0;
}

int sum_series(int n){
    int sum = 0;
    if(n == 0)
        return 0;
    
    else
        return n+sum_series(n-1);
    }
    

