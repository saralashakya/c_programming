#include <stdio.h>

int main(){
    char text[20];
    printf("Enter the text");
    gets(text);
    int i = 0;
    int count =0;
    while(text[i]!='\0'){
        if(text[i]=='a' || text[i]=='e'|| text[i]=='i' || text[i]=='o' || text[i]=='u' ){
            count++;
        }
        
        i++;
    }
    printf("The total number of vowels is %d", count);
    return 0;
}