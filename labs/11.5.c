// WAP to enter name, age, and run made by players
// and store them in file , until the user presses n.
// Read the content of the file and display the name
// of those player who made the run greater than 100.

#include <stdio.h>
struct player
{
    char name[20];
    int age;
    int run;
};

int main()
{
    char choice;
    FILE *fp = fopen("player.bin", "wb");
    struct player p;

    do
    {
        printf("Enter player's name: ");
        scanf("%s", p.name);
        printf("Enter player's age: ");
        scanf("%d", &p.age);
        printf("Enter runs made by the player: ");
        scanf("%d", &p.run);

        // fprintf(fp,"%s%d%d\n", player.name, player.age, player.runs);
        fwrite(&p, sizeof(p), 1, fp);
        printf("Do you want to enter another player? (y/n): ");
        scanf("%c", &choice); // Notice the space before %c to consume any newline character left in the buffer
    } while (choice == 'y' || choice == 'Y');
    fclose(fp);
    fp = fopen("player.bin", "rb");
    printf("Name of players who make run greater than 100\n");
    while (fread(&p, sizeof(p), 1, fp) == 1)
    {
        if (p.run > 100)
        {
            printf("Player Name: %s\n", p.name);
        }
    }
    return 0;
}