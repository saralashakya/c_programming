#include <stdio.h>
#include <math.h>

int main(){

    int n,digit = 0,num_copy,remainder,sum = 0;
    printf("Enter the number to check whether the number is armstrong or not\n");
    scanf("%d",&n);
    num_copy = n;
    //finding out the digit of the number
    while(num_copy!=0){
        digit++;
        num_copy = num_copy/10;
    }
    num_copy = n;
    while(num_copy!=0){
        remainder = num_copy % 10;
        sum = sum + pow(remainder,digit);
        num_copy = num_copy/10;
    }

    if (sum == n){
        printf("%d is an armstrong number",n);
    }
    else{
        printf("%d is not an armstrong number",n);

    }
    
    return 0;
}