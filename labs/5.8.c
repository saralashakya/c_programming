#include <stdio.h>
int power(int,int);

int  main(){
int x = 2, y = 4;
int result = power(x,y);
printf("%d power %d is %d",x,y,result);
return 0;
}

int power(int x,int y){
    static int po = 1;
    if(y == 0)
        return po;
    else{
       po = po * x;
       return power(x,y-1);
    }
    
}