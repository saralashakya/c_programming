#include <stdio.h>

int main() {
    int num1, num2, num3, largest;

    // Prompt the user to enter three numbers
    printf("Enter three numbers: ");
    scanf("%d%d%d",&num1,&num2,&num3);

    // Compare the three numbers using conditional operator
    largest = (num1 > num2) ? ((num1 > num3) ? num1 : num3) : ((num2 > num3) ? num2 : num3);

    // Print the largest number
    printf("The largest number is: %d\n", largest);

    return 0;
}