#include <stdio.h>

int main(){
    int num,i,sum=0,num_copy,rem,fact;
    printf("Enter the number to check whether the number is strong or not\n");
    scanf("%d",&num);
    num_copy = num;
    while(num_copy!=0){
        rem = num_copy%10;
        fact = 1;
        for(i = 1;i<= rem; i++){
            fact = fact*i;
        }
        sum = sum+fact;
        num_copy= num_copy/10;
    }
    if(sum == num){
        printf("%d is a strong number",num);
    }
    else{
        printf("%d is not a strong number",num);
    }
    return 0;
}