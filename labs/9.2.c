#include <stdio.h>
#include <stdlib.h>
void sort(int *q, int n);

int main() {
    int *p, i, j, t, n;

    printf("Enter no of data: ");
    scanf("%d", &n);

    p = (int *)malloc(n * sizeof(*p));
    if (p == NULL) {
        printf("Memory allocation failed\n");
        return 1;
    }

    for (i = 0; i < n; i++) {
        printf("Enter the %d number: ", i + 1);
        scanf("%d", (p + i));
    }
    sort(p,n);
    
    printf("The numbers in ascending order are: ");
    for (i = 0; i < n; i++) {
        printf("%d ", *(p + i));
    }
    printf("\n");

    free(p);
    return 0;
}

void sort(int *q, int n){
    int t;
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (*(q + i) > *(q + j)) {
                t = *(q + i);
                *(p + i) = *(p + j);
                *(p + j) = t;
            }
        }
    }

}

