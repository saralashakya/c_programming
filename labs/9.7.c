#include <stdio.h>
#include <stdlib.h>
void add(int *p, int *q, int *r, int row, int col);

int main(){
    int *p, *q, *r, row, col;
    printf("Enter the row of the matrix");
    scanf("%d",&row);
    printf("Enter the col of the matrix");
    scanf("%d",&col);
    p = malloc(row*col*sizeof(int));
    q = malloc(row*col*sizeof(int));
    r = malloc(row*col*sizeof(int));


    printf("Enter the elements for matrix 1");
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            scanf("%d",(p+i*col+j));
        }
    }
    printf("Enter the elements for matrix 2");
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            scanf("%d",(q+i*col+j));
        }
    }
    add(p,q,r,row,col);
    return 0;
}

void add(int *p, int *q, int *r, int row, int col){
    //performing matrix addition
    for(int i = 0 ; i < row; i++){
        for(int j = 0; j < col; j++){
            *(r+i*col+j) = *(p+i*col+j) + *(q+i*col+j);
        }
    }

    //printing matrix addition
    for(int i = 0 ; i < row; i++){
        for(int j = 0; j < col; j++){
          printf("%d",*(r+i*col+j));
        }
        printf("\n");
    }

}
