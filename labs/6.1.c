#include <stdio.h>
#define size 20
int main() {
   int num[size], sum = 0;
   int n;
   printf("How many numbers?\n");
   scanf("%d",&n);

   // Scanning the numbers from the user
   for (int i = 0; i < n; i++) {
       printf("Enter number %d: ", i + 1);
       scanf("%d", &num[i]);
   }

   // Calculating the sum of factorials of each number
   for (int i = 0; i < n; i++) {
       int fact = 1;
       // Calculating factorial
       for(int j = 1; j <= num[i]; j++) {
            fact = fact * j;
       }
       // Adding factorial to the sum
       sum = sum + fact;
   }

   printf("Sum = %d\n", sum);
   return 0;
}