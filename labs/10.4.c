#include <stdio.h>
struct student{
    int roll;
    char name[50];
    char address[100];
};
void scan_data(struct student *s1);
void print_data(struct student *s1);

int main(){
    struct student s1;

    printf("Enter the information of student");
    scan_data(&s1);
    printf("The information of the student is as follows:\n");
    print_data(&s1);
    return 0;
}

void scan_data(struct student *s1){
    printf("Enter roll of student\n");
    scanf("%d",&s1->roll);
    printf("Enter name of student\n");
    scanf("%s",s1->name);
    printf("Enter address of student\n");
    scanf("%s",s1->address);

}
void print_data(struct student *s1){
    printf("%d\t%s\t%s",s1->roll,s1->name,s1->address);

}
