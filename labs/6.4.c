#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 10 // Maximum size of matrices

int main()
{
    int matrix1[MAX_SIZE][MAX_SIZE], matrix2[MAX_SIZE][MAX_SIZE], sum[MAX_SIZE][MAX_SIZE];
    int rows1, cols1, rows2, cols2, i, j;

    printf("Enter the number of rows and columns for the first matrix: ");
    scanf("%d%d", &rows1, &cols1);

    printf("Enter the number of rows and columns for the second matrix: ");
    scanf("%d%d", &rows2, &cols2);

    // Check if the dimensions of the matrices are equal
    if (rows1 != rows2 && cols1 != cols2)
    {
        printf("Error! Number of rows and columns of both matrices should be equal for addition.\n");
        exit(0); 
    }

    // Input for the first matrix
    printf("Enter elements of the first matrix:\n");
    for (i = 0; i < rows1; i++)
    {
        for (j = 0; j < cols1; j++)
        {
            scanf("%d", &matrix1[i][j]);
        }
    }

    // Input for the second matrix
    printf("Enter elements of the second matrix:\n");
    for (i = 0; i < rows2; i++)
    {
        for (j = 0; j < cols2; j++)
        {
            scanf("%d", &matrix2[i][j]);
        }
    }

    // Adding the matrices
    for (i = 0; i < rows1; i++)
    {
        for (j = 0; j < cols1; j++)
        {
            sum[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    // Displaying the resultant matrix
    printf("Resultant matrix after addition:\n");
    for (i = 0; i < rows1; i++)
    {
        for (j = 0; j < cols1; j++)
        {
            printf("%d ", sum[i][j]);
        }
        printf("\n");
    }

    return 0;
}
