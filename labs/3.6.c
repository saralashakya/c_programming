#include <stdio.h>

int main()
{
    int num, i, isPrime; 

    // Input the number
    for (num = 1; num <= 20; num++)
    {
        isPrime = 1;
        // Check for prime
        if (num <= 1)
        {
            isPrime = 0;
        }
        else
        {
            for (i = 2; i <= num / 2; i++)
            {
                if (num % i == 0)
                {
                    isPrime = 0;
                    break;
                }
            }
        }

        // Display result
        if (isPrime)
        {
            printf("%d is a prime number.\n", num);
        }
        else
        {
            printf("%d is not a prime number.\n", num);
        }
    }

    return 0;
}