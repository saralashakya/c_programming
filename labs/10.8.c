#include <stdio.h>

struct ingredient
{
    char name[30];
    int quantity;
};

struct momo
{
    char size[20];
    char type[20];
    int count;
    int price;
    struct ingredient ing[10];
};

int main()
{
    struct momo m1[10];
    int n;
    printf("How many customers");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        printf("Enter the detail of momo that customer %d want to order\n", i+1);
        printf("Enter the size of momo\n");
        scanf("%s", m1[i].size);
        printf("Enter the type of momo\n");
        char a = getchar();
        fgets(m1[i].type, sizeof(m1[i].type), stdin);
        printf("Enter total no of momo that you want to eat\n");
        scanf("%d", &m1[i].count);
        printf("Enter price of momo\n");
        scanf("%d", &m1[i].price);
        printf("How many ingredients do you want to add");
        scanf("%d", &n);
        for (int j = 0; j < n; j++)
        {
            char a = getchar(); // for clearing the buffer
            printf("Enter the name of the ingredient\n");
            gets(m1[i].ing[j].name);
            printf("Enter the quantity of ingredient\n");
            scanf("%d", &m1[i].ing[i].quantity);
        }
    }

    printf("The details of the momo that customers have ordered is as follows\n");
    for(int i = 0; i < n; i++){
        printf("customer %d\n",i+1);
        printf("size: %s\n", m1[i].size);
        printf("type: %s\n", m1[i].type);
        printf("count: %d\n", m1[i].count);
        printf("price: %d\n", m1[i].price);
        printf("The ingredients that you have added are as follows\n");
        for (int j = 0; j < n; j++)
        {
            printf("%s\t", m1[i].ing[j].name);
            printf("%d\n", m1[i].ing[j].quantity);
        }

    }
    
    return 0;
}