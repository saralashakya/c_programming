#include <stdio.h>
int main(){
    int a[3][3], b[3][3], c[3][3], i, j, k, sum;
    printf("Enter the elements of matrix A");
    for(i =0; i < 3; i++){
        for(j = 0; j< 3; j++){
            scanf("%d",*(a+i)+j);
        }
    }

    printf("Enter the elements of matrix B");
    for(i =0; i < 3; i++){
            for(j = 0; j< 3; j++){
                scanf("%d",*(b+i)+j);
            }
    }
    //performing matrix multiplication using pointer 
    for(i = 0; i < 3; i++){
        for(j = 0; j < 3; j++){
            sum = 0;
            for(k = 0; k < 3; k++){
                sum = sum + *(*(a+i)+k) *  *(*(b+k)+j);
            }
            *(*(c+i)+j) = sum;
        }
    }

    //printing matrix A
    for(i =0; i < 3; i++){
        for(j = 0; j< 3; j++){
            printf("%d",*(*(a+i)+j));
        }
        printf("\n");
    }

    //printing matrix B
    for(i =0; i < 3; i++){
        for(j = 0; j< 3; j++){
            printf("%d",*(*(b+i)+j));
        }
         printf("\n");
    }

    //printing matrix C
    for(i =0; i < 3; i++){
        for(j = 0; j< 3; j++){
            printf("%d",*(*(c+i)+j));
        }
         printf("\n");
    }

    return 0;
}