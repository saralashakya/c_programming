// Write a program to allocate memory for 3*3 matrix using DMA. 
// Prompt the user to input the elements in the matrix and  
// find the largest element of the matrix.

#include <stdio.h>
#include <stdlib.h>
int main(){
    int *p,row, col, largest;
    printf("Enter the row of the matrix");
    scanf("%d",&row);
    printf("Enter the col of the matrix");
    scanf("%d",&col);
    p = malloc((row*col)*sizeof(int));

    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            scanf("%d",p+(i*col)+j);
        }
    }
    largest = *(p);
    printf("Enter the elements in the matrix");
    for(int i = 0; i < row; i++){
        for(int j = 0; j < col; j++){
            if(*(p+i*col+j)> largest){
                largest = *(p+i*col+j);
            }
        }
    }
    printf("The largest element is %d",largest);
    free(p);

    return 0;
}