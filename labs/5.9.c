#include <stdio.h>
void fibo(int n);

int main(){

    int n;
    printf("Enter the number of terms");
    scanf("%d", &n);
   fibo(n);

    return 0;
}

void fibo(int n){
int t1 = 0, t2 = 1, next_term;

for(int i = 1; i<=n; i++){
    next_term = t1+t2;
    printf("%d",t1);
    t1 = t2;
    t2 = next_term;
}
}