#include <stdio.h>
#define size 20
int main()
{
   int num[size],temp;
   int n;
   printf("How many numbers?\n");
   scanf("%d",&n);


   // Scanning the numbers from the user
   for (int i = 0; i < n; i++)
   {
       printf("Enter number %d: ", i + 1);
       scanf("%d", &num[i]);
   }

   // Before sorting the numbers
   for (int i = 0; i < n; i++)
   {
       printf("%d\t",num[i]);
   }
   printf("\n");


    for(int i = 0;i < n-1;i++){
        for(int j=i+1;j<n;j++ ){
            if(num[i]>num[j]){
                temp = num[i];
                num[i]=num[j];
                num[j]=temp;
            }
        }

    }
    // After sorting the numbers in an ascending order

    for(int i = 0;i<n;i++){
        printf("%d\t",num[i]);
    }
}
