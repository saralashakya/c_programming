#include <stdio.h>
#include <stdlib.h>

int main(){
    int *p, *q, *r, row1, col1, row2, col2,sum;
    printf("Enter the row of the matrix1");
    scanf("%d",&row1);
    printf("Enter the col of the matrix1");
    scanf("%d",&col1);
    printf("Enter the row of the matrix2");
    scanf("%d",&row2);
    printf("Enter the col of the matrix2");
    scanf("%d",&col2);

    if(col1!=row2){
        printf("Matrix Multiplication is not possible");
        exit(1);
    }

    p = malloc(row1*col1*sizeof(int));
    q = malloc(row2*col2*sizeof(int));
    r = malloc(row1*col2*sizeof(int));


    printf("Enter the elements for matrix 1");
    for(int i = 0; i < row1; i++){
        for(int j = 0; j < col1; j++){
            scanf("%d",(p+i*col1+j));
        }
    }
    printf("Enter the elements for matrix 2");
    for(int i = 0; i < row2; i++){
        for(int j = 0; j < col2; j++){
            scanf("%d",(q+i*col2+j));
        }
    }
    //performing matrix multiplication
    for(int i = 0 ; i < row1; i++){
        for(int j = 0; j < col2; j++){
            sum = 0;
           for(int k = 0; k < col1; k++){
            sum = sum + *(p+(i*col1+k)) * *(q+(k*col1)+j);
           }
           *(r+ (i * col2)+j) =  sum;
        }
    }

    //printing matrix addition
    for(int i = 0 ; i < row1; i++){
        for(int j = 0; j < col2; j++){
          printf("%d",*(r+i*col2+j));
        }
        printf("\n");
    }



    return 0;
}