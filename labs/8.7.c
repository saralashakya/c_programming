#include <stdio.h>

void scan_matrix(int a[][3]);
void add_matrix(int a[][3], int b[][3], int c[][3]);
void print_matrix(int a[][3]);
int main(){
    int a[3][3], b[3][3], c[3][3], i, j;
    printf("Enter the elements of matrix A");
    scan_matrix(a);
    printf("Enter the elements of matrix B");
    scan_matrix(b);
    add_matrix(a,b,c);
    print_matrix(a);
    print_matrix(b);
    print_matrix(c);


    
    return 0;
}

void scan_matrix(int a[][3]){
    for(int i =0; i < 3; i++){
        for(int j = 0; j< 3; j++){
            scanf("%d",*(a+i)+j);
        }
    }
}

void add_matrix(int a[][3], int b[][3], int c[][3]){
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            *(*(c+i)+j) = *(*(a+i)+j)+ *(*(b+i)+j);
        }
    }    
}

void print_matrix(int a[][3]){
    for(int i =0; i < 3; i++){
        for(int j = 0; j< 3; j++){
            printf("%d",*(*(a+i)+j));
        }
        printf("\n");
    }
}