#include <stdio.h>
struct student{
    int roll;
    char name[50];
    char address[100];
};
void scan_data(struct student s1[], int n);
void print_data(struct student s1[], int n);
int main(){
    struct student s1[10];
    int n;
    printf("Enter the no of information of the students that you want to store\n");
    scanf("%d",&n);
    printf("Enter the information of the students\n");
    scan_data(s1,n);
    printf("The information of the students are as follows\n");
    print_data(s1,n);
    return 0;
}

void scan_data(struct student s1[], int n){
    for(int i = 0 ; i < n; i++){
        printf("Enter the information of student %d\n",i+1);
        printf("Enter roll of student");
        scanf("%d",&s1[i].roll);
        printf("Enter name of student");
        scanf("%s",s1[i].name);
        printf("Enter address of student");
        scanf("%s",s1[i].address);
    }

}
void print_data(struct student s1[], int n){
    for(int i = 0; i< n; i++){
        printf("%d\t%s\t%s",s1[i].roll,s1[i].name, s1[i].address);
        printf("\n");

    }

}