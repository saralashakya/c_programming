#include <stdio.h>

struct ingredient
{
    char name[30];
    int quantity;
};

struct momo
{
    char size[20];
    char type[20];
    int count;
    int price;
    struct ingredient ing[10];
};

int main()
{
    struct momo m1;
    int n;

    printf("Enter the detail of momo that you want to order\n");
    printf("Enter the size of momo\n");
    scanf("%s", m1.size);
    printf("Enter the type of momo\n");
    char a = getchar();
    fgets(m1.type,sizeof(m1.type),stdin);
    printf("Enter total no of momo that you want to eat\n");
    scanf("%d", &m1.count);
    printf("Enter price of momo\n");
    scanf("%d", &m1.price);
    printf("How many ingredients do you want to add");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        char a = getchar(); // for clearing the buffer
        printf("Enter the name of the ingredient\n");
        gets(m1.ing[i].name);
        printf("Enter the quantity of ingredient\n");
        scanf("%d", &m1.ing[i].quantity);
    }

    printf("The details of the momo that you have ordered is as follows\n");
    printf("size: %s\n", m1.size);
    printf("type: %s\n", m1.type);
    printf("count: %d\n", m1.count);
    printf("price: %d\n", m1.price);
    printf("The ingredients that you have added are as follows\n");
    for (int i = 0; i < n; i++)
    {
        printf("%s\t", m1.ing[i].name);
        printf("%d\n", m1.ing[i].quantity);
    }

    return 0;
}