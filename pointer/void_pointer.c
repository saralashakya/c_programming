#include <stdio.h>

int main() {
    int a = 10;
    char b = 'x';

    void *p = &a;  // void pointer holds address of int 'a'
    printf("%d\n", *(int *)p);

    return 0;
}