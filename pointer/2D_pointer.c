#include <stdio.h>
int main(){
  int mat1[3][3];

  //scanning elements in the 2 D array *(mat1+i) gives the address of that particular 
  //row and then +j will will address to that particular element in that particular row
  for(int i = 0; i < 3; i++){
    for(int j = 0; j<3;j++){
        scanf("%d",*(mat1+i)+j);  
    }
  }

  for(int i = 0; i < 3; i++){
    for(int j = 0; j<3;j++){
        printf("%d",*(*(mat1+i)+j));  
    }
  }

  return 0;
}