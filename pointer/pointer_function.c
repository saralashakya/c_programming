#include <stdio.h>
void swap(int *a, int *b) {
   int temp = *a;
   *a = *b;
   *b = temp;
}
int main() {
   int *px, *py, x = 4, y = 2;
   px = &x; py = &y;

   printf("Before swapping, x = %d, y = %d\n", x, y);
   swap(px, py);
   printf("After swapping, x = %d, y = %d\n", x, y);
   swap(&x, &y);
   printf("After swapping again, x = %d, y = %d\n", x, y);
}
