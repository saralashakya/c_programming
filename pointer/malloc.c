#include <stdio.h>
#include <stdlib.h>
int main(){

    int *p, n, i;
    float sum = 0, avg;
    printf("Enter no. of data to find the sum and average");
    scanf("%d",&n);
    p = (int *)malloc(n*sizeof(int));
    
    for(i = 0; i < n; i++){
        scanf("%d", (p+i));
    }

    for(i = 0; i < n; i++){
       sum = sum + *(p+i);
    }
    avg = sum / n;
    printf("The sum is %f and average is %f",sum, avg);

    return 0;
}