#include <stdio.h>

int main() {
   int arr[] = {2, 3, 4, 5, 6};

   long n = sizeof(arr) / sizeof(int);

   for (int i = 0; i < n; i++) {
       printf("i=%d \t arr[i]=%d \t *(arr+i)=%d \t &arr[i]=%p \t (arr+i)=%p \n",
           i ,arr[i], *(arr + i), &arr[i], (arr + i));
   }
}
