    #include "graphics.h"

    int main() {
        int gd = DETECT, gm;
        initgraph(&gd, &gm, "c:\\tc\\bgi");
        circle(200,300,50);
        ellipse(100,100,0,360,50,75);
        rectangle(0,0,100,200);
        rectangle(150,250,200,300);
        getch();
        closegraph();
        return 0;
    }