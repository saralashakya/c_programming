    #include "graphics.h"

    int main() {
        int gd = DETECT, gm;
        initgraph(&gd, &gm, NULL);
        int mx,my,x,y;
        mx=getmaxx();
        my=getmaxy();
        x=mx/2;
        y=mx/2;
        circle(x,y,50);
        circle(x,y,75);
        getch();
        closegraph();
        return 0;
    }