#include <stdio.h>

int main() {
    // putchar('H');  // Outputs the character 'H'
    // putchar('e');  // Outputs the character 'e'
    // putchar('l');  // Outputs the character 'l'
    // putchar('l');  // Outputs the character 'l'
    // putchar('o');  // Outputs the character 'o'
    // putchar('\n'); // Outputs a newline character
    char a;
    putchar("E");
    
    a = getchar();
    putchar(a);

    return 0;
}