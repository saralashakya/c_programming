// C program to implement 
// getch() function 
#include <conio.h>
#include <stdio.h> 
// Driver code 
int main() 
{ 
    printf("Enter any character: "); 
  
    char ch = getch();
    printf("%c",ch);
  
    return 0; 
}