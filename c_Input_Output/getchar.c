// C program to implement the getchar() function 
#include <stdio.h> 
  
int main() 
{ 
    char c=getchar();
    printf("%c",c);
    return 0; 
}